extern crate arrayvec;
extern crate byteorder;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate log;
extern crate try_from;

use {
    arrayvec::ArrayVec,
    byteorder::{
        LittleEndian,
        ReadBytesExt,
    },
    std::{
        io,
        io::Read,
        fs::File,
        path::Path,
    },
    try_from::TryFrom,
};

#[derive(Debug)]
pub struct Pst {
    header: Header,
    type_: Format,
}

#[derive(Debug)]
pub struct Header {
    offset_index_1: u64,
    offset_index_2: u64,
    back_ptr_1: u64,
    back_ptr_2: u64,
}


#[derive(Debug, Fail)]
pub enum HeaderParseError {
    #[fail(display = "invalid header signature {:#X}", _0)]
    InvalidFileSignature(u32),
    #[fail(display = "unrecognized index format {:#X}", _0)]
    UnrecognizedFormat(u8),
    #[fail(display = "unrecognized encryption cipher constant {:#X}", _0)]
    UnrecognizedEncryption(u8),
    #[fail(display = "invalid client signature {:#X}", _0)]
    InvalidClientSignature(u16),
    #[fail(display = "invalid client format {:#X}", _0)]
    UnrecognizedClientFormat(u16),
    #[fail(display = "could not access input file: {}", _0)]
    CouldNotAccessFile(io::Error),
    #[fail(display = "could not read input file")]
    CouldNotReadFile,
}

impl From<io::Error> for HeaderParseError {
    fn from(e: io::Error) -> Self {
        HeaderParseError::CouldNotAccessFile(e)
    }
}

#[derive(Debug)]
pub enum Encryption {
    None,
    SimpleSubstitution,
    ThreeRotorEnigma,
}

impl TryFrom<u8> for Encryption {
    type Err = ();
    fn try_from(x: u8) -> Result<Encryption, ()> {
        use Encryption::*;
        Ok(match x {
            0 => None,
            1 => SimpleSubstitution,
            2 => ThreeRotorEnigma,
            _ => return Err(()),
        })
    }
}

#[derive(Debug)]
enum Format {
    Ansi,
    Unicode,
}

impl TryFrom<u8> for Format {
    type Err = ();
    fn try_from(x: u8) -> Result<Self, ()> {
        use Format::*;
        Ok(match x {
            0x0E | 0x0F => Ansi,
            0x16 | 0x17 => Unicode,
            _ => return Err(()),
        })
    }
}

pub fn parse(p: impl AsRef<Path>) -> Result<Header, HeaderParseError> {
    use HeaderParseError::*;

    debug!("Parsing a PST header...");

    let mut file = File::open(p)?;
    let mut buf = ArrayVec::<[u8; 768]>::new();
    file.read(&mut buf).map_err(CouldNotAccessFile)?;

    const FILE_SIGNATURE: u32 = 0x4E444221;
    let signature = file.read_u32::<LittleEndian>()?;
    if signature != FILE_SIGNATURE {
        return Err(InvalidFileSignature(signature));
    }

    let _crc_lower = file.read_u32::<LittleEndian>()?;
    debug!("  _crc_lower: {:#X}", _crc_lower);

    const CLIENT_SIGNATURE: u16 = 0x4D53;
    let client_signature = file.read_u16::<LittleEndian>()?;
    if client_signature != CLIENT_SIGNATURE {
        return Err(InvalidClientSignature(client_signature));
    }

    let format = {
        let f = file.read_u8()?;
        Format::try_from(f)
            .map_err(|_| UnrecognizedFormat(f))?
    };
    debug!("  format: {:#?}", format);

    let encryption = {
        let f = file.read_u8()?;
        Encryption::try_from(f)
            .map_err(|_| UnrecognizedEncryption(f))?
    };
    debug!("  encryption: {:#?}", encryption);

    const ONLY_KNOWN_CLIENT_FORMAT: u16 = 0x0013;
    let client_format = file.read_u16::<LittleEndian>()?;
    if client_format != ONLY_KNOWN_CLIENT_FORMAT {
        return Err(UnrecognizedClientFormat(client_format));
    }

    unimplemented!();

    // let total_file_size =

    // let header = Header {

    // };

    // Ok(Pst {
    //     header,
    //     type_,
    // })
}
