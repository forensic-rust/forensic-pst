extern crate forensic_pst;
#[macro_use]
extern crate quicli;

use {
    quicli::prelude::*,
    std::path::PathBuf,
};

#[derive(Debug, StructOpt)]
struct Cli {
    #[structopt(short = "i", long = "input", parse(from_os_str))]
    input_path: PathBuf,
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbosity: u8,
}

main!(|args: Cli, log_level: verbosity| {
    println!("pst: {:#?}", forensic_pst::parse(args.input_path)?);
});
