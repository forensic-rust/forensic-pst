Helpful links found so far:

## `five-ten-sg.com`'s article

Found here: http://www.five-ten-sg.com/libpst/rn01re05.html.

### 2018-05-23

This article is really handy, since it has several hex dumps of different parts
of the PST file. It glosses over several chunks of the hex dumps, though, which
is a problem if we want to ensure the entire file has been consumed.

@erichdongubler created a couple of data files from the header
dumps here at `src/test/data`.

## `file-recovery.com`'s article

Found here: http://www.file-recovery.com/pst-signature-format.htm.

### 2018-05-23

This article cleared up many of the missing header chunks in
`five-ten-sg.com`'s article, though it's much harder to follow the differences
between the PST formats.
